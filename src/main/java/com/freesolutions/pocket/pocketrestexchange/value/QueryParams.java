package com.freesolutions.pocket.pocketrestexchange.value;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class QueryParams {

    private static final ConcurrentMap<String, DateTimeFormatter> FORMATTER_CACHE = new ConcurrentHashMap<>();

    private static final DateTimeFormatter DEFAULT_LD_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd");
    private static final DateTimeFormatter DEFAULT_LT_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter DEFAULT_LDT_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");

    @Nonnull
    public final String queryString;
    @Nonnull
    public final Map<String, Object> params;

    @Nonnull
    public static QueryParams of(@Nonnull RestParams restParams) {
        return new QueryParams(restParams);
    }

    @Nonnull
    public static QueryParams of(@Nullable Object request) {
        if (request instanceof QueryParams) {
            return (QueryParams) request;
        }
        if (request instanceof RestParams) {
            return new QueryParams((RestParams) request);
        }
        return new QueryParams(request);
    }

    @Override
    public String toString() {
        return "QueryParams[queryString: " + queryString + "]" + params;
    }

    private QueryParams(@Nullable RestParams restParams) {

        //short case
        if (restParams == null) {
            this.queryString = "";
            this.params = Collections.emptyMap();
            return;
        }

        StringBuilder qb = new StringBuilder();
        Map<String, Object> vars = new LinkedHashMap<>();
        for (Map.Entry<String, Object> entry : restParams.params.entrySet()) {

            //determine name
            String name = entry.getKey();
            if (name.isEmpty()) {
                continue;
            }

            //retrieve value
            Object value = entry.getValue();

            //append
            appendEntry(qb, vars, name, value, null, null);
        }
        this.queryString = qb.toString();
        this.params = Collections.unmodifiableMap(vars);
    }

    private QueryParams(@Nullable Object request) {
        StringBuilder qb = new StringBuilder();
        Map<String, Object> vars = new LinkedHashMap<>();
        if (request != null) {
            for (Field field : request.getClass().getDeclaredFields()) {

                //filter fields
                if (field.isSynthetic()) {
                    continue;
                }
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }

                //determine name
                String name = null;
                JsonProperty jsonProperty = field.getAnnotation(JsonProperty.class);
                if (jsonProperty != null) {
                    name = jsonProperty.value();
                }
                if (name == null || name.isEmpty()) {
                    name = field.getName();
                }

                //determine pattern
                //NOTE:
                // Pattern supported only at root level of field!
                // Not supported at generic parameter level of multi-values.
                String pattern = null;
                JsonFormat jsonFormat = field.getAnnotation(JsonFormat.class);
                if (jsonFormat != null) {
                    pattern = jsonFormat.pattern();
                }

                //retrieve value
                Object value;
                try {
                    value = field.get(request);
                } catch (Throwable t) {
                    throw new RuntimeException(t);
                }

                //append
                appendEntry(qb, vars, name, value, pattern, null);
            }
        }
        this.queryString = qb.toString();
        this.params = Collections.unmodifiableMap(vars);
    }

    private static void appendEntry(
        @Nonnull StringBuilder qb,
        @Nonnull Map<String, Object> vars,
        @Nonnull String name,
        @Nullable Object value,
        @Nullable String pattern,
        @Nullable String elementName
    ) {

        //skip null values for non-elements
        if (elementName == null && value == null) {
            return;
        }

        //multi-values processing
        if (elementName == null) {
            //NOTE: here "value" is not null

            //handle arrays
            if (value.getClass().isArray()) {
                int l = Array.getLength(value);
                for (int i = 0; i < l; i++) {
                    Object element = Array.get(value, i);
                    appendEntry(
                        qb,
                        vars,
                        name,
                        element,
                        pattern,
                        multiName(name, i)
                    );
                }
                return;
            }

            //handle collections
            if (value instanceof Collection) {
                int i = 0;
                for (Object element : (Collection<?>) value) {
                    appendEntry(
                        qb,
                        vars,
                        name,
                        element,
                        pattern,
                        multiName(name, i)
                    );
                    i++;
                }
                return;
            }
        }

        //default case
        if (qb.length() == 0) {
            qb.append('?');
        } else {
            qb.append('&');
        }
        String varName = elementName != null ? elementName : name;
        qb.append(name).append('=').append('{').append(varName).append('}');
        vars.put(varName, value != null ? serializeValue(value, pattern) : "");
    }

    @Nonnull
    private static String multiName(@Nonnull String name, int i) {
        return name + "." + i;
    }

    @Nonnull
    private static Object serializeValue(@Nonnull Object value, @Nullable String pattern) {
        if (value instanceof Enum) {
            return ((Enum<?>) value).name();
        }
        if (value instanceof LocalDate) {
            if (pattern == null) {
                return ((LocalDate) value).format(DEFAULT_LD_FORMATTER);
            }
            return ((LocalDate) value).format(getFormatter(pattern));
        }
        if (value instanceof LocalTime) {
            if (pattern == null) {
                return ((LocalTime) value).format(DEFAULT_LT_FORMATTER);
            }
            return ((LocalTime) value).format(getFormatter(pattern));
        }
        if (value instanceof LocalDateTime) {
            if (pattern == null) {
                return ((LocalDateTime) value).format(DEFAULT_LDT_FORMATTER);
            }
            return ((LocalDateTime) value).format(getFormatter(pattern));
        }
        checkArgument(pattern == null, "no expected pattern for value: %s, pattern: %s", value, pattern);
        return value;
    }

    @Nonnull
    private static DateTimeFormatter getFormatter(@Nonnull String pattern) {
        DateTimeFormatter result = FORMATTER_CACHE.get(pattern);
        if (result == null) {
            synchronized (FORMATTER_CACHE) {
                result = FORMATTER_CACHE.get(pattern);
                if (result == null) {
                    result = DateTimeFormatter.ofPattern(pattern);
                    FORMATTER_CACHE.put(pattern, result);
                }
            }
        }
        return result;
    }
}
