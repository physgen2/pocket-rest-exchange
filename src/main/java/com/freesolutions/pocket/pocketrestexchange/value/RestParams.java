package com.freesolutions.pocket.pocketrestexchange.value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class RestParams {

    public static final RestParams EMPTY = new RestParams(Collections.emptyMap());

    @Nonnull
    public final Map<String, Object> params;

    public static class Builder {

        @Nonnull
        private final Map<String, Object> params;
        private boolean completed;

        private Builder(@Nonnull Map<String, Object> params) {
            this.params = params;
        }

        //single values (nullable)
        //---------------------------------------

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable String value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public <E extends Enum<E>> Builder addNullable(@Nonnull String name, @Nullable E value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable LocalDate value, @Nonnull DateTimeFormatter formatter) {
            return value != null ? add(name, value, formatter) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable LocalTime value, @Nonnull DateTimeFormatter formatter) {
            return value != null ? add(name, value, formatter) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable LocalDateTime value, @Nonnull DateTimeFormatter formatter) {
            return value != null ? add(name, value, formatter) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable Integer value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable Long value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable Float value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable Double value) {
            return value != null ? add(name, value) : this;
        }

        @Nonnull
        public Builder addNullable(@Nonnull String name, @Nullable Boolean value) {
            return value != null ? add(name, value) : this;
        }

        //single values (nonnull)
        //---------------------------------------

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull String value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkNotNull(value, "value must be !null");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        @Nonnull
        public <E extends Enum<E>> Builder add(@Nonnull String name, @Nonnull E value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkNotNull(value, "value must be !null");
            checkState(!completed);
            params.put(name, value.name());
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull LocalDate value, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkNotNull(value, "value must be !null");
            checkState(!completed);
            params.put(name, value.format(formatter));
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull LocalTime value, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkNotNull(value, "value must be !null");
            checkState(!completed);
            params.put(name, value.format(formatter));
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull LocalDateTime value, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkNotNull(value, "value must be !null");
            checkState(!completed);
            params.put(name, value.format(formatter));
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, int value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, long value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, float value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, double value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, boolean value) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, value);
            return this;
        }

        //arrays
        //---------------------------------------

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull String... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public <E extends Enum<E>> Builder add(@Nonnull String name, @Nonnull E[] values) {//explicit array of enums, ok
            return addEnums(name, Arrays.asList(values));
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull DateTimeFormatter formatter, @Nonnull LocalDate... values) {
            return addLocalDates(name, Arrays.asList(values), formatter);
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull DateTimeFormatter formatter, @Nonnull LocalTime... values) {
            return addLocalTimes(name, Arrays.asList(values), formatter);
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull DateTimeFormatter formatter, @Nonnull LocalDateTime... values) {
            return addLocalDateTimes(name, Arrays.asList(values), formatter);
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull Integer... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull Long... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull Float... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull Double... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder add(@Nonnull String name, @Nonnull Boolean... values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        //collections
        //---------------------------------------

        @Nonnull
        public Builder addStrings(@Nonnull String name, @Nonnull Collection<String> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public <E extends Enum<E>> Builder addEnums(@Nonnull String name, @Nonnull Collection<E> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values.stream().map(Enum::name).collect(Collectors.toList()));
            return this;
        }

        @Nonnull
        public Builder addLocalDates(@Nonnull String name, @Nonnull Collection<LocalDate> values, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values.stream().map(x -> x.format(formatter)).collect(Collectors.toList()));
            return this;
        }

        @Nonnull
        public Builder addLocalTimes(@Nonnull String name, @Nonnull Collection<LocalTime> values, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values.stream().map(x -> x.format(formatter)).collect(Collectors.toList()));
            return this;
        }

        @Nonnull
        public Builder addLocalDateTimes(@Nonnull String name, @Nonnull Collection<LocalDateTime> values, @Nonnull DateTimeFormatter formatter) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values.stream().map(x -> x.format(formatter)).collect(Collectors.toList()));
            return this;
        }

        @Nonnull
        public Builder addIntegers(@Nonnull String name, @Nonnull Collection<Integer> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder addLongs(@Nonnull String name, @Nonnull Collection<Long> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder addFloats(@Nonnull String name, @Nonnull Collection<Float> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder addDoubles(@Nonnull String name, @Nonnull Collection<Double> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        @Nonnull
        public Builder addBooleans(@Nonnull String name, @Nonnull Collection<Boolean> values) {
            checkArgument(!name.isEmpty(), "name must be !empty");
            checkState(!completed);
            params.put(name, values);
            return this;
        }

        //---------------------------------------

        @Nonnull
        public RestParams build() {
            checkState(!completed);
            this.completed = true;
            return new RestParams(Collections.unmodifiableMap(params));
        }
    }

    @Nonnull
    public static Builder builder() {
        return new Builder(new LinkedHashMap<>());
    }

    @Nonnull
    public static RestParams empty() {
        return EMPTY;
    }

    @Nonnull
    public Builder modify() {
        return new Builder(new LinkedHashMap<>(params));//make copy of params, ok
    }

    @Override
    public String toString() {
        return "RestParams" + params;
    }

    private RestParams(@Nonnull Map<String, Object> params) {
        this.params = params;
    }
}
