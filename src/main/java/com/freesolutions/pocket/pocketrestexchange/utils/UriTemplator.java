package com.freesolutions.pocket.pocketrestexchange.utils;

import com.freesolutions.pocket.pocketrestdata.utils.UriEncoder;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Stanislau Mirzayeu
 */
public class UriTemplator {

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("\\{([^/]+?)\\}");

    @Nonnull
    public static String template(@Nonnull String value, @Nonnull Map<String, ?> vars) {
        if (value.indexOf('{') == -1) {
            return value;
        }
        if (value.indexOf(':') != -1) {
            value = clear(value);
        }
        Matcher matcher = PLACEHOLDER_PATTERN.matcher(value);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String match = matcher.group(1);
            int i = match.indexOf(':');
            String name = i >= 0 ? match.substring(0, i) : match;
            Object obj = vars.get(name);
            if (obj == null) {
                throw new IllegalArgumentException(
                    "Placeholder \"" + name + "\" required, but no variable found for it" +
                        ", value: " + value +
                        ", vars: " + vars
                );
            }
            matcher.appendReplacement(sb, UriEncoder.encode(obj.toString()));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    @Nonnull
    private static String clear(@Nonnull String source) {
        StringBuilder sb = new StringBuilder();
        int level = 0;
        int l = source.length();
        for (int i = 0; i < l; i++) {
            char c = source.charAt(i);
            if (c == '{') {
                level++;
            }
            if (c == '}') {
                level--;
            }
            if (level > 1) {
                continue;
            }
            if (level <= 0 || c != '}') {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
