package com.freesolutions.pocket.pocketrestexchange.component;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 * <p>
 * Abstraction to define serialization of request & response body in {@link RestExchange}.
 * </p>
 */
@FunctionalInterface
public interface BodySerializer {

    BodySerializer RAW_BYTES = new BodySerializer() {

        @Nonnull
        @Override
        public byte[] serialize(@Nonnull Object value) throws Exception {
            checkArgument(value instanceof byte[], "expected byte[] value");
            return (byte[]) value;
        }
    };

    BodySerializer UTF8_STRING = new BodySerializer() {

        @Nonnull
        @Override
        public byte[] serialize(@Nonnull Object value) throws Exception {
            checkArgument(value instanceof String, "expected String value");
            return ((String) value).getBytes(StandardCharsets.UTF_8);
        }
    };

    @Nonnull
    static BodySerializer rawBytes() {
        return RAW_BYTES;
    }

    @Nonnull
    static BodySerializer utf8String() {
        return UTF8_STRING;
    }

    @Nonnull
    byte[] serialize(@Nonnull Object value) throws Exception;
}
