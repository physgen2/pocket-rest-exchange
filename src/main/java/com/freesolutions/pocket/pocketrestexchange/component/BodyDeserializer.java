package com.freesolutions.pocket.pocketrestexchange.component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 * <p>
 * Abstraction to define deserialization of request & response body in {@link RestExchange}.
 * </p>
 */
@FunctionalInterface
public interface BodyDeserializer {

    BodyDeserializer RAW_BYTES = new BodyDeserializer() {

        @Nullable
        @Override
        public <BODY> BODY deserialize(@Nonnull InputStream inputStream, @Nonnull Object type) throws Exception {
            checkArgument(type == byte[].class, "expected byte[] type");
            return (BODY) inputStream.readAllBytes();
        }
    };

    BodyDeserializer UTF8_STRING = new BodyDeserializer() {

        @SuppressWarnings("unchecked")
        @Nullable
        @Override
        public <BODY> BODY deserialize(@Nonnull InputStream inputStream, @Nonnull Object type) throws Exception {
            checkArgument(type == String.class, "expected String type");
            return (BODY) new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        }
    };

    @Nonnull
    static BodyDeserializer rawBytes() {
        return RAW_BYTES;
    }

    @Nonnull
    static BodyDeserializer utf8String() {
        return UTF8_STRING;
    }

    @Nullable
    <BODY> BODY deserialize(@Nonnull InputStream inputStream, @Nonnull Object type) throws Exception;
}
