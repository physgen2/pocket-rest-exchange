package com.freesolutions.pocket.pocketrestexchange.component;

import com.freesolutions.pocket.pocketrestdata.dto.IResponse;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Stanislau Mirzayeu
 * <p>
 * Abstraction to define conversion from raw response object to final result object in {@link RestExchange}.
 * </p>
 */
@FunctionalInterface
public interface R2RConverter<RESULT, RESPONSE> {

    R2RConverter<?, ?> DEFAULT = IResponse::resultOf;

    @SuppressWarnings("unchecked")
    @Nonnull
    static <RESULT, RESPONSE> R2RConverter<RESULT, RESPONSE> def() {
        return (R2RConverter<RESULT, RESPONSE>) DEFAULT;
    }

    @Nullable
    RESULT toResult(@Nullable RESPONSE value) throws Exception;
}
