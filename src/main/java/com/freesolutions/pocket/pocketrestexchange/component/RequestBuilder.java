package com.freesolutions.pocket.pocketrestexchange.component;

import com.freesolutions.pocket.pocketrestdata.exception.RestExchangeException;
import com.freesolutions.pocket.pocketrestdata.value.Headers;
import com.freesolutions.pocket.pocketrestdata.value.HttpMethod;
import com.freesolutions.pocket.pocketrestexchange.value.RestParams;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class RequestBuilder {

    private static final String DEFAULT_PATH_TEMPLATE = "/";
    private static final HttpMethod DEFAULT_HTTP_METHOD = HttpMethod.GET;

    @Nonnull
    private final RestExchange restExchange;
    @Nonnull
    private final String endpoint;
    @Nullable
    private HttpMethod httpMethod;
    @Nullable
    private String pathTemplate;
    @Nullable
    private RestParams pathParams;
    @Nullable
    private Object queryRequest;
    @Nullable
    private Object bodyRequest;
    @Nullable
    private Headers headers;
    @Nullable
    private BodySerializer bodySerializer;
    @Nullable
    private BodyDeserializer bodyDeserializer;
    @Nullable
    private Object responseType;
    @Nullable
    private R2RConverter<?, ?> r2rConverter;

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder method(@Nonnull HttpMethod httpMethod, @Nonnull String pathTemplate) {
        this.httpMethod = httpMethod;
        this.pathTemplate = pathTemplate;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder pathParams(@Nonnull RestParams pathParams) {
        this.pathParams = pathParams;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder queryRequest(@Nonnull Object queryRequest) {
        this.queryRequest = queryRequest;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder bodyRequest(@Nonnull Object bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder headers(@Nonnull Headers headers) {
        this.headers = headers;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder bodySerializer(@Nonnull BodySerializer bodySerializer) {
        this.bodySerializer = bodySerializer;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder bodyDeserializer(@Nonnull BodyDeserializer bodyDeserializer) {
        this.bodyDeserializer = bodyDeserializer;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder responseType(@Nonnull Object responseType) {
        this.responseType = responseType;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RequestBuilder r2rConverter(@Nonnull R2RConverter<RESULT, RESPONSE> r2rConverter) {
        this.r2rConverter = r2rConverter;
        return this;
    }

    @Nonnull
    public <RESULT, RESPONSE> RESULT exchangeNonnull() throws RestExchangeException {
        return checkNotNull(doExchange(false));
    }

    @Nullable
    public <RESULT, RESPONSE> RESULT exchangeNullable() throws RestExchangeException {
        return doExchange(true);
    }

    protected RequestBuilder(@Nonnull RestExchange restExchange, @Nonnull String endpoint) {
        this.restExchange = restExchange;
        this.endpoint = endpoint;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    private <RESULT, RESPONSE> RESULT doExchange(boolean resultIsNullable) throws RestExchangeException {
        return restExchange.doExchange(
            endpoint,
            httpMethod != null ? httpMethod : DEFAULT_HTTP_METHOD,
            pathTemplate != null ? pathTemplate : DEFAULT_PATH_TEMPLATE,
            pathParams,
            queryRequest,
            bodyRequest,
            headers,
            bodySerializer != null ? bodySerializer : restExchange.bodySerializer,
            bodyDeserializer != null ? bodyDeserializer : restExchange.bodyDeserializer,
            responseType,
            r2rConverter != null ? (R2RConverter<RESULT, RESPONSE>) r2rConverter : R2RConverter.def(),
            resultIsNullable
        );
    }
}
