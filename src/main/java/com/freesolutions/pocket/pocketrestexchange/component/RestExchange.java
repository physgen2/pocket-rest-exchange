package com.freesolutions.pocket.pocketrestexchange.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freesolutions.pocket.pocketrestdata.exception.RestExchangeException;
import com.freesolutions.pocket.pocketrestdata.value.Headers;
import com.freesolutions.pocket.pocketrestdata.value.HttpMethod;
import com.freesolutions.pocket.pocketrestexchange.builder.DefaultBuilder;
import com.freesolutions.pocket.pocketrestexchange.utils.UriTemplator;
import com.freesolutions.pocket.pocketrestexchange.value.QueryParams;
import com.freesolutions.pocket.pocketrestexchange.value.RestParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class RestExchange {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestExchange.class);

    @Nonnull
    public final CloseableHttpClient httpClient;
    @Nonnull
    public final BodySerializer bodySerializer;
    @Nonnull
    public final BodyDeserializer bodyDeserializer;

    public volatile boolean logExchanges;
    public volatile boolean logErrors;

    /**
     * @deprecated use directly {@link DefaultBuilder}
     */
    @Nonnull
    public static RestExchange buildDefault(
        @Nonnull ObjectMapper objectMapper,
        long connectRequestTimeoutMs,
        long connectTimeoutMs,
        long readTimeoutMs,
        int maxConnections,
        boolean logExchanges,
        boolean logErrors
    ) {
        RestExchange restExchange = DefaultBuilder.defaultRestExchange(
            objectMapper,
            connectRequestTimeoutMs,
            connectTimeoutMs,
            readTimeoutMs,
            maxConnections,
            -1L //connectionTtlMs
        );
        restExchange.logExchanges = logExchanges;
        restExchange.logErrors = logErrors;
        return restExchange;
    }

    public RestExchange(
        @Nonnull CloseableHttpClient httpClient,
        @Nonnull BodySerializer bodySerializer,
        @Nonnull BodyDeserializer bodyDeserializer
    ) {
        this.httpClient = httpClient;
        this.bodySerializer = bodySerializer;
        this.bodyDeserializer = bodyDeserializer;
    }

    @Nonnull
    public RequestBuilder endpoint(@Nonnull String endpoint) {
        return new RequestBuilder(this, endpoint);
    }

    @Nullable
    protected <RESULT, RESPONSE> RESULT doExchange(
        @Nonnull String endpoint,
        @Nonnull HttpMethod httpMethod,
        @Nonnull String pathTemplate,
        @Nullable RestParams pathParams,
        @Nullable Object queryRequest,
        @Nullable Object bodyRequest,
        @Nullable Headers headers,
        @Nonnull BodySerializer bodySerializer,
        @Nonnull BodyDeserializer bodyDeserializer,
        @Nullable Object responseType,
        @Nonnull R2RConverter<RESULT, RESPONSE> r2rConverter,
        boolean resultIsNullable
    ) throws RestExchangeException {

        //query params
        QueryParams queryParams = null;
        if (queryRequest != null) {
            try {
                queryParams = QueryParams.of(queryRequest);
            } catch (Throwable t) {
                throw RestExchangeException.ofCauseWithMessage(
                    "Failed to build query params" +
                        ", endpoint: " + endpoint +
                        ", httpMethod: " + httpMethod +
                        ", pathTemplate: " + pathTemplate +
                        (pathParams != null ? ", pathParams: " + pathParams : "") +
                        ", queryRequest: " + queryRequest +
                        (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                        ", headers: " + headers +
                        ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null),
                    t
                );
            }
        }

        //url
        URI url;
        try {
            Map<String, ?> vars = toVariables(queryParams, pathParams);
            String queryString = queryParams != null ? queryParams.queryString : "";
            url = new URI(endpoint + UriTemplator.template(pathTemplate + queryString, vars));
        } catch (Throwable t) {
            throw RestExchangeException.ofCauseWithMessage(
                "Failed to build url" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null),
                t
            );
        }

        //request body
        byte[] requestBytes = null;
        try {
            if (bodyRequest != null) {
                checkArgument(!(bodyRequest instanceof QueryParams), "<QueryParams> is not allowed to usage for body request");
                requestBytes = bodyRequest instanceof RestParams ?
                    bodySerializer.serialize(((RestParams) bodyRequest).params) :
                    bodySerializer.serialize(bodyRequest);
            }
        } catch (Throwable t) {
            throw RestExchangeException.ofCauseWithMessage(
                "failed to build request body" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null),
                t
            );
        }

        //do call
        StatusAware<RESPONSE> callResult;
        try {
            callResult = doCall(
                url,
                httpMethod,
                requestBytes,
                headers,
                bodyDeserializer,
                responseType
            );
        } catch (Throwable t) {
            throw RestExchangeException.ofCauseWithMessage(
                "Rest call error" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null),
                t
            );
        }

        //check invalid status
        if (!is2xxSuccessful(callResult.status)) {
            RESPONSE response = callResult.response;
            throw RestExchangeException.ofResultWithMessage(
                callResult.status,
                response,
                "Invalid http-status" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null)
            );
        }

        //check answer presence
        RESPONSE response = callResult.response;
        if (responseType != null && response == null) {
            throw RestExchangeException.ofResultWithMessage(
                callResult.status,
                null,
                "Absent answer body" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null)
            );
        }

        //retrieve result from response
        RESULT result;
        try {
            result = r2rConverter.toResult(response);
        } catch (Throwable t) {
            throw RestExchangeException.ofResultWithMessage(
                callResult.status,
                response,
                "Failed to retrieve result from response" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null)
            );
        }

        //check result
        if (!resultIsNullable && result == null) {
            throw RestExchangeException.ofResultWithMessage(
                callResult.status,
                response,
                "Absent result, while expected" +
                    ", endpoint: " + endpoint +
                    ", httpMethod: " + httpMethod +
                    ", pathTemplate: " + pathTemplate +
                    (pathParams != null ? ", pathParams: " + pathParams : "") +
                    (queryRequest != null ? ", queryRequest: " + queryRequest : "") +
                    (bodyRequest != null ? ", bodyRequest: " + bodyRequest : "") +
                    ", headers: " + headers +
                    ", responseType: " + (responseType != null ? typeReferenceName(responseType) : null)
            );
        }

        return result;
    }

    @Nonnull
    protected <RESPONSE> StatusAware<RESPONSE> doCall(
        @Nonnull URI uri,
        @Nonnull HttpMethod httpMethod,
        @Nullable byte[] requestBytes,
        @Nullable Headers headers,
        @Nonnull BodyDeserializer bodyDeserializer,
        @Nullable Object responseType
    ) throws Exception {

        HttpUriRequest request;
        try {

            //create request
            request = createRequest(httpMethod, uri, requestBytes != null);

            //setup headers
            if (headers != null) {
                headers.forEach(request::addHeader);
            }

            //setup body
            if (requestBytes != null) {
                checkState(request instanceof HttpEntityEnclosingRequest);
                ((HttpEntityEnclosingRequest) request)
                    .setEntity(new ByteArrayEntity(requestBytes, ContentType.APPLICATION_JSON));
            }
        } catch (Throwable t) {
            if (logErrors) {
                LOGGER.error(
                    "REST ERROR >>> Failed to build request" +
                        ", " + t //don't log fully stack trace
                );
            }
            throw t;
        }

        CloseableHttpResponse response = null;
        try {

            //execute & retrieve statusCode
            int statusCode;
            HttpEntity httpEntity;
            try {
                response = httpClient.execute(request);
                statusCode = response.getStatusLine().getStatusCode();
                httpEntity = response.getEntity();
            } catch (Throwable t) {
                if (logErrors) {
                    LOGGER.error(
                        "REST ERROR >>> Execution failed" +
                            ", " + toString(request, requestBytes) +
                            ", " + t //don't log fully stack trace
                    );
                }
                throw t;
            }

            //read response bytes
            byte[] responseBytes = null;
            boolean logRequired = logExchanges || (logErrors && !is2xxSuccessful(statusCode));
            try {
                if (logRequired) {
                    responseBytes = httpEntity != null ? EntityUtils.toByteArray(httpEntity) : null;
                    if (responseBytes == null) {
                        responseBytes = new byte[0];
                    }
                }
            } catch (Throwable t) {
                if (logErrors) {
                    LOGGER.error(
                        "REST ERROR >>> Response reading failed" +
                            ", " + toString(request, requestBytes) +
                            ", " + t //don't log fully stack trace
                    );
                }
                throw t;
            }

            //response IS
            InputStream responseIS = null;
            if (responseType != null) {
                try {
                    if (responseBytes != null) {
                        responseIS = new ByteArrayInputStream(responseBytes);
                    } else {
                        responseIS = httpEntity != null ?
                            httpEntity.getContent() :
                            new ByteArrayInputStream(new byte[0]);
                    }
                } catch (Throwable t) {
                    if (logErrors) {
                        LOGGER.error(
                            "REST ERROR >>> Response reading failed" +
                                ", " + toString(request, requestBytes) +
                                ", " + t //don't log fully stack trace
                        );
                    }
                    throw t;
                }
            }

            //parse response
            StatusAware<RESPONSE> result;
            try {
                RESPONSE responseObj = responseIS != null ?
                    bodyDeserializer.deserialize(responseIS, checkNotNull(responseType)) :
                    null;
                result = new StatusAware<>(statusCode, responseObj);
            } catch (Throwable t) {
                if (logErrors) {
                    if (responseBytes != null) {
                        LOGGER.error(
                            "REST ERROR >>> Response parsing failed" +
                                ", " + toString(request, requestBytes) +
                                ", " + toString(response, responseBytes) +
                                ", " + t //don't log fully stack trace
                        );
                    } else {
                        LOGGER.error(
                            "REST ERROR >>> Response parsing failed" +
                                ", " + toString(request, requestBytes) +
                                ", " + t //don't log fully stack trace
                        );
                    }
                }
                throw t;
            }

            //log exchanges / errors if necessary
            if (logRequired) {
                checkNotNull(responseBytes);
                if (is5xxServerError(statusCode)) {
                    LOGGER.error(
                        "REST ERROR >>> Invalid http-status, {}, {}",
                        toString(request, requestBytes),
                        toString(response, responseBytes)
                    );
                } else if (!is2xxSuccessful(statusCode)) {
                    LOGGER.warn(
                        "REST ERROR >>> Invalid http-status, {}, {}",
                        toString(request, requestBytes),
                        toString(response, responseBytes)
                    );
                } else {
                    LOGGER.info(
                        "REST EXCHANGE >>> {}, {}",
                        toString(request, requestBytes),
                        toString(response, responseBytes)
                    );
                }
            }

            return result;
        } finally {
            if (response != null) {
                try {
                    try {
                        EntityUtils.consume(response.getEntity());
                    } finally {
                        response.close();
                    }
                } catch (Throwable ignored) {
                }
            }
        }
    }

    @Nonnull
    private static HttpUriRequest createRequest(@Nonnull HttpMethod httpMethod, @Nonnull URI uri, boolean withBody) {
        switch (httpMethod) {
            case GET:
                return !withBody ?
                    new HttpGet(uri) :
                    new HttpEntityEnclosingRequestBase() {
                        {
                            setURI(uri);
                        }

                        public String getMethod() {
                            return HttpGet.METHOD_NAME;
                        }
                    };
            case POST:
                return new HttpPost(uri);
            case PUT:
                return new HttpPut(uri);
            case PATCH:
                return new HttpPatch(uri);
            case DELETE:
                return !withBody ?
                    new HttpDelete(uri) :
                    new HttpEntityEnclosingRequestBase() {
                        {
                            setURI(uri);
                        }

                        public String getMethod() {
                            return HttpDelete.METHOD_NAME;
                        }
                    };
            default:
                throw new IllegalArgumentException("Invalid HttpMethod: " + httpMethod);
        }
    }

    @Nonnull
    private static Map<String, ?> toVariables(@Nullable QueryParams queryParams, @Nullable RestParams pathParams) {
        if (queryParams != null && !queryParams.params.isEmpty()
            && pathParams != null && !pathParams.params.isEmpty()) {
            Map<String, Object> uriVariables = new HashMap<>(queryParams.params);
            uriVariables.putAll(pathParams.params);
            return uriVariables;
        }
        if (queryParams != null && !queryParams.params.isEmpty()) {
            return queryParams.params;
        }
        if (pathParams != null && !pathParams.params.isEmpty()) {
            return pathParams.params;
        }
        return Collections.emptyMap();
    }

    private static boolean is2xxSuccessful(int status) {
        return status / 100 == 2;
    }

    private static boolean is5xxServerError(int status) {
        return status / 100 == 5;
    }

    @Nonnull
    private static String toString(
        @Nonnull HttpUriRequest request,
        @Nullable byte[] requestBytes
    ) {
        return "Request[" +
            "uri: " + request.getURI() +
            ", method: " + request.getMethod() +
            ", headers: " + Arrays.toString(request.getAllHeaders()) +
            ", body(UTF-8): " + (requestBytes != null ? new String(requestBytes, StandardCharsets.UTF_8) : null) +
            "]";
    }

    @Nonnull
    private static String toString(
        @Nonnull HttpResponse response,
        @Nonnull byte[] responseBytes
    ) throws IOException {
        StringBuilder body = new StringBuilder();
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                new ByteArrayInputStream(responseBytes),
                StandardCharsets.UTF_8
            )
        );
        String line = reader.readLine();
        while (line != null) {
            body.append(line);
            body.append('\n');
            line = reader.readLine();
        }
        StatusLine sl = response.getStatusLine();
        return "Response[" +
            "status(code/reason): " + sl.getStatusCode() + " / " + sl.getReasonPhrase() +
            ", headers: " + Arrays.toString(response.getAllHeaders()) +
            ", body(UTF-8): " + body +
            "]";
    }

    @Nonnull
    private static String typeReferenceName(@Nonnull Object type) {
        //heuristic: try to guess structure of type, based on popular technique for <TypeReference>s
        Type typeOfTypeRef = type.getClass().getGenericSuperclass();
        if (!(typeOfTypeRef instanceof ParameterizedType)) {
            return type.getClass().getTypeName();
        }
        Type[] typeArgs = ((ParameterizedType) typeOfTypeRef).getActualTypeArguments();
        if (typeArgs.length == 0) {
            return type.getClass().getTypeName();
        }
        return typeArgs[0].getTypeName();
    }

    @Immutable
    private static class StatusAware<RESPONSE> {

        public final int status;
        @Nullable
        public final RESPONSE response;

        public StatusAware(int status, @Nullable RESPONSE response) {
            this.status = status;
            this.response = response;
        }
    }
}
