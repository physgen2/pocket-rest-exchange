package com.freesolutions.pocket.pocketrestexchange.builder;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freesolutions.pocket.pocketrestexchange.component.BodyDeserializer;
import com.freesolutions.pocket.pocketrestexchange.component.BodySerializer;
import com.freesolutions.pocket.pocketrestexchange.component.RestExchange;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketrestexchange.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
public class DefaultBuilder {

    @Nonnull
    public static RestExchange defaultRestExchange(
        @Nonnull ObjectMapper objectMapper,
        long connectRequestTimeoutMs,
        long connectTimeoutMs,
        long readTimeoutMs,
        int maxConnections,
        long connectionTtlMs
    ) {
        return new RestExchange(
            defaultHttpClientBuilder(
                connectRequestTimeoutMs,
                connectTimeoutMs,
                readTimeoutMs,
                maxConnections,
                connectionTtlMs
            ).build(),
            defaultBodySerializer(objectMapper),
            defaultBodyDeserializer(objectMapper)
        );
    }

    @Nonnull
    public static HttpClientBuilder defaultHttpClientBuilder(
        long connectRequestTimeoutMs,
        long connectTimeoutMs,
        long readTimeoutMs,
        int maxConnections,
        long connectionTtlMs
    ) {
        HttpClientBuilder builder = HttpClients.custom();
        builder.setMaxConnTotal(maxConnections);
        builder.setMaxConnPerRoute(maxConnections);
        builder.setConnectionTimeToLive(connectionTtlMs, TimeUnit.MILLISECONDS);
        builder.disableAuthCaching();
        builder.disableAutomaticRetries();
        builder.disableCookieManagement();
        builder.disableRedirectHandling();
        builder.setDefaultRequestConfig(
            RequestConfig.custom()
                .setConnectionRequestTimeout((int) connectRequestTimeoutMs)
                .setConnectTimeout((int) connectTimeoutMs)
                .setSocketTimeout((int) readTimeoutMs)
                .build()
        );
        return builder;
    }

    @Nonnull
    public static BodySerializer defaultBodySerializer(
        @Nonnull ObjectMapper objectMapper
    ) {
        return value -> objectMapper.writeValueAsBytes(checkNotNull(value));
    }

    @Nonnull
    public static BodyDeserializer defaultBodyDeserializer(
        @Nonnull ObjectMapper objectMapper
    ) {
        return new BodyDeserializer() {
            @SuppressWarnings("unchecked")
            @Nullable
            @Override
            public <BODY> BODY deserialize(@Nonnull InputStream inputStream, @Nonnull Object type) throws Exception {
                checkArgument(type instanceof TypeReference<?>, "expected TypeReference for ObjectMapper");
                JsonParser parser = objectMapper.getFactory().createParser(inputStream);
                return parser.nextToken() != null ? //skip parsing if no input
                    (BODY) objectMapper.readValue(parser, (TypeReference<?>) type) :
                    null;
            }
        };
    }
}
