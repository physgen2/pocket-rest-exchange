### What is it
___
This is pocket library to provide fast & easy work with http-json-queries
to external world. Purpose is to provide most typical approach to make queries,
with standard serialization / deserialization & ability to provide
arguments / headers / body to request, then easily get deserialized response.

You need this library when you have many microservices with typical active
interaction via http-json between them, and you need unification of your code.

### RestExchange
___
Example of typical configuration.

Properties file:
```properties
#-- rest: some-application-api
#--------------------------------------------------------------------------------------------------
rest.properties.some-application-api.connect-timeout=10000
rest.properties.some-application-api.read-timeout=60000
rest.properties.some-application-api.max-connections=5
rest.properties.some-application-api.log-exchanges=false
rest.properties.some-application-api.log-errors=false
```
Configuration in code:
```java
@Configuration
@ParametersAreNonnullByDefault
public class RestConfig {

    @Bind
    @Qualifier("some-application-api")
    public RestExchange restExchangeForSomeApplicationApi(
        ObjectMapper objectMapper, //defined in another place & injected here just for example 
        @Property("rest.properties.some-application-api.connect-timeout") long connectTimeoutMs,
        @Property("rest.properties.some-application-api.read-timeout") long readTimeoutMs,
        @Property("rest.properties.some-application-api.max-connections") int maxConnections,
        @Property("rest.properties.some-application-api.log-exchanges") boolean logExchanges,
        @Property("rest.properties.some-application-api.log-errors") boolean logErrors
    ) {
        return RestExchange.buildDefault(
            Json.objectMapper(),
            connectTimeoutMs,
            connectTimeoutMs,
            readTimeoutMs,
            maxConnections,
            logExchanges,
            logErrors
        );
    }

    //... other RestExchanges
}
```
Usually you need distinct `RestExchange` for every target external API
(e.g., API of microservice), that have particular `host`:`port`. In properties
file necessary properties are defined &  injected to configure every necessary
`RestExchange`.

Note, that `RestExchange` is not enough to correctly organize queries in
your code. This is kind of *Engine*, that is used to make queries, but you
still need implement logic of your queries (with endpoint host, path,
parameters, headers, body, ...), that organized as *Client*. Thus, *Client*
is abstraction to work with external world, using *Engine* inside it, so that
this abstraction is encapsulated for any user's code.

### Client singleton

Example of *Client* singleton, that must be provided for any particular instance of
`RestExchange`

Properties file:
```properties
#-- endpoints
#--------------------------------------------------------------------------------------------------
endpoint.some-application-api=http://node.some-application:8002
```
Class of singleton *Client*:
```java
@Bind
@ParametersAreNonnullByDefault
public class SomeApplicationApiClient {

    private final RestExchange restExchange;
    private final String endpoint;

    public SomeApplicationApiClient(
        @Qualifier("some-application-api") RestExchange restExchange,
        @Property("endpoint.some-application-api") String endpoint
    ) {
        this.restExchange = restExchange;
        this.endpoint = endpoint;
    }

    //---------------------------------------------------------------------------------------------

    @Nullable
    public Activity findActivity(ActivityId activityId) {
        ActivityView activityView = restExchange.exchangeNullable(
            endpoint,
            "/activities/{activityId}",
            HttpMethod.GET,
            RestParams.builder() //pathParams
                .add("activityId", activityId.value)
                .build(),
            null, //queryRequest
            null, //bodyRequest
            null, //headers
            new TypeReference<ApiResponse<ActivityView>>() {
            }
        );
        return activityView != null ?
            ActivityView.toActivity(activityView) :
            null;
    }

    //... other methods, that can be called in external API
}
```

*Client* provides API at level of code, that make abstraction  of interaction
with external API. User's code just make requests to *Client*, providing
necessary arguments, and get some result as response. Interface of client is
completely at level of code, this is primary goal of *Client* as idiom.

Every external API usually have `endpoint`, which is starting part of url with
hostname, port, ..., before request path. This `endpoint` is injected from
properties, that is place where refernce to external API is configured. This
is typical configuration approach, just follow it and be happy.

Note usage of DTO class `ActivityView` in this example. This is business
specific DTO, and must be serializable / deserializable in terms of json
library `jackson-databind`, that is typical and most popular approach, which
require special json-annotations on fields of such DTOs.

See also at `ApiResponse` usages. This is not mandatiry to use it, but usually
due to unification in many microservices this DTO structure is used to reduce
complexity of code. See `ApiResponse` in `pocket-rest-data` - this is basic
json-wrapper for  any responses. You can avoid this and use what you want in
response. `ApiResponse` is just popular practice, that nice to have ready to
use in your hands.

### Dependencies
___
Library have next dependencies:

- `jsr305`

- `slf4j-api`

- `jackson-databind`

- `http-client`

- `pocket-rest-data`

These dependencies must be provided in classpath of final application.
